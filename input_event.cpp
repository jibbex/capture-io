#include "input_event.hpp"
#include "qdebug.h"

namespace capture_io::events {

InputEvent::InputEvent(std::uint64_t info,
                       unsigned long flags,
                       std::uint64_t msg) noexcept
    : _msg { msg },
      _extra_info { info },
      _flags { flags } {}

InputEvent::~InputEvent() {}

InputEvent::operator const std::string() noexcept {
    return msg_to_string(_msg);
}

MouseEvent::MouseEvent(unsigned int info,
                       unsigned long flags,
                       std::uint64_t msg,
                       unsigned long data,
                       const Point& point) noexcept
    : InputEvent(info, flags, msg),
      _data { data },
      _point { point } {}

MouseEvent::MouseEvent(const MSLLHOOKSTRUCT& data, std::uint64_t msg) noexcept
    : InputEvent(data.dwExtraInfo, data.flags, msg),
      _data { data.mouseData },
      _point { data.pt.x, data.pt.y } {}

MouseEvent::operator const MSLLHOOKSTRUCT() noexcept {
	return {
		POINT { _point.x, _point.y },
		_data, get_flags(), {}, get_extra_info()
	};
}

MouseEvent::operator const std::string() noexcept {
	std::string str { std::to_string(_point.x) };
	str.append(", ").append(std::to_string(_point.y))
		.append(" ").append(msg_to_string(get_msg()));

	return str;
}

KeyboardEvent::KeyboardEvent(unsigned int info,
                             unsigned long flags,
                             std::uint64_t msg,
                             unsigned short vkey,
                             unsigned short key_code) noexcept
    : InputEvent(info, flags, msg),
      _vkey { vkey },
      _key_code { key_code } {}

KeyboardEvent::KeyboardEvent(const KBDLLHOOKSTRUCT& data, std::uint64_t msg) noexcept
    : InputEvent(data.dwExtraInfo, data.flags, msg),
      _vkey { static_cast<unsigned short>(data.vkCode) },
      _key_code { static_cast<unsigned short>(data.scanCode) } {}

KeyboardEvent::operator const std::string() noexcept {
	std::string str { vk_to_string(_vkey) };
	str.append(" ").append(msg_to_string(get_msg()));

	return str;
}

KeyboardEvent::operator const KBDLLHOOKSTRUCT() noexcept {
	return {
		_vkey, _key_code, get_flags(),
		{}, get_extra_info()
	};
}

QKeySequence vk_to_keyseq(const std::uint32_t key) noexcept {
	QString str {};

	switch (key) {
		case VK_BACK:
			str.append("Backspace" );
			break;
		case VK_RETURN:
			str.append("Return");
			break;
		case VK_SHIFT:
			str.append("Shift+");
			break;
		case VK_CONTROL:
			str.append("Ctrl+");
			break;
		case VK_MENU:
			str.append("Alt+");
			break;
		case VK_PAUSE:
			str.append("Pause");
			break;
		case VK_CAPITAL:
			str.append("Caps");
			break;
		case VK_ESCAPE:
			str.append("Esc");
			break;
		case VK_SPACE:
			str.append("Space");
			break;
		case VK_PRIOR:
			str.append("PgUp");
			break;
		case VK_NEXT:
			str.append("PgDown");
			break;
		case VK_END:
			str.append("End");
			break;
		case VK_HOME:
			str.append("Home");
			break;
		case VK_LEFT:
			str.append("Left");
			break;
		case VK_UP:
			str.append("Up");
			break;
		case VK_RIGHT:
			str.append("Right");
			break;
		case VK_DOWN:
			str.append("Down");
			break;
		case VK_SELECT:
			str.append("Select");
			break;
		case VK_PRINT:
			str.append("Print");
			break;
		case VK_EXECUTE:
			str.append("Exec");
			break;
		case VK_SNAPSHOT:
			str.append("Print");
			break;
		case VK_INSERT:
			str.append("Insert");
			break;
		case VK_DELETE:
			str.append("Del");
			break;
		case VK_HELP:
			str.append("Help");
			break;
		case '0': [[fallthrough]];
		case '1': [[fallthrough]];
		case '2': [[fallthrough]];
		case '3': [[fallthrough]];
		case '4': [[fallthrough]];
		case '5': [[fallthrough]];
		case '6': [[fallthrough]];
		case '7': [[fallthrough]];
		case '8': [[fallthrough]];
		case '9':
			str.append(char(key));
			break;
		case 'A': [[fallthrough]];
		case 'B': [[fallthrough]];
		case 'C': [[fallthrough]];
		case 'D': [[fallthrough]];
		case 'E': [[fallthrough]];
		case 'F': [[fallthrough]];
		case 'G': [[fallthrough]];
		case 'H': [[fallthrough]];
		case 'I': [[fallthrough]];
		case 'J': [[fallthrough]];
		case 'K': [[fallthrough]];
		case 'L': [[fallthrough]];
		case 'M': [[fallthrough]];
		case 'N': [[fallthrough]];
		case 'O': [[fallthrough]];
		case 'P': [[fallthrough]];
		case 'Q': [[fallthrough]];
		case 'R': [[fallthrough]];
		case 'S': [[fallthrough]];
		case 'T': [[fallthrough]];
		case 'U': [[fallthrough]];
		case 'V': [[fallthrough]];
		case 'W': [[fallthrough]];
		case 'X': [[fallthrough]];
		case 'Y': [[fallthrough]];
		case 'Z':
			str.append(char(key));
			break;
		case VK_NUMPAD0:
			str.append("Num0");
			break;
		case VK_NUMPAD1:
			str.append("Num1");
			break;
		case VK_NUMPAD2:
			str.append("Num2");
			break;
		case VK_NUMPAD3:
			str.append("Num3");
			break;
		case VK_NUMPAD4:
			str.append("Num4");
			break;
		case VK_NUMPAD5:
			str.append("Num5");
			break;
		case VK_NUMPAD6:
			str.append("Num6");
			break;
		case VK_NUMPAD7:
			str.append("Num7");
			break;
		case VK_NUMPAD8:
			str.append("Num8");
			break;
		case VK_NUMPAD9:
			str.append("Num9");
			break;
		case VK_MULTIPLY:
			str.append("*");
			break;
		case VK_ADD:
			str.append("+");
			break;
		case VK_SEPARATOR:
			str.append("|");
			break;
		case VK_SUBTRACT:
			str.append("-");
			break;
		case VK_DECIMAL:
			str.append("\\");
			break;
		case VK_DIVIDE:
			str.append("/");
			break;
		case VK_F1: [[fallthrough]];
		case VK_F2: [[fallthrough]];
		case VK_F3: [[fallthrough]];
		case VK_F4: [[fallthrough]];
		case VK_F5: [[fallthrough]];
		case VK_F6: [[fallthrough]];
		case VK_F7: [[fallthrough]];
		case VK_F8: [[fallthrough]];
		case VK_F9: [[fallthrough]];
		case VK_F10: [[fallthrough]];
		case VK_F11: [[fallthrough]];
		case VK_F12: [[fallthrough]];
		case VK_F13: [[fallthrough]];
		case VK_F14: [[fallthrough]];
		case VK_F15: [[fallthrough]];
		case VK_F16: [[fallthrough]];
		case VK_F17: [[fallthrough]];
		case VK_F18: [[fallthrough]];
		case VK_F19: [[fallthrough]];
		case VK_F20: [[fallthrough]];
		case VK_F21: [[fallthrough]];
		case VK_F22: [[fallthrough]];
		case VK_F23: [[fallthrough]];
		case VK_F24:
			str.append("F");
			str.append(std::to_string(key - VK_F1 + 1));
			break;
		case VK_NUMLOCK:
			str.append("NumLock");
			break;
		case VK_SCROLL:
			str.append("Scroll");
			break;
		case VK_LSHIFT: [[fallthrough]];
		case VK_RSHIFT:
			str.append("Shift+");
			break;
		case VK_LCONTROL: [[fallthrough]];
		case VK_RCONTROL:
			str.append("Ctrl+");
			break;
		case VK_LMENU: [[fallthrough]];
		case VK_RMENU:
			str.append("Alt+");
			break;
	}

	return { str };
}

std::string vk_to_string(const std::uint32_t key) noexcept {
	switch (key) {
		case VK_LBUTTON:
			return { "Left mouse button" };
		case VK_RBUTTON:
			return { "Right mouse button" };
		case VK_CANCEL:
			return { "Control-break processing" };
		case VK_MBUTTON:
			return { "Middle mouse button" };
		case VK_XBUTTON1:
			return { "X1 mouse button" };
		case VK_XBUTTON2:
			return { "X2 mouse button" };
		case 0x07:
			return { "Reserved" };
		case VK_BACK:
			return { "BACKSPACE key" };
		case VK_TAB:
			return { "TAB key" };
		case 0x0A: [[fallthrough]];
		case 0x0B:
			return { "Reserved" };
		case VK_CLEAR:
			return { "CLEAR key" };
		case VK_RETURN:
			return { "ENTER key" };
		case 0x0E: [[fallthrough]];
		case 0x0F:
			return { "Unassigned" };
		case VK_SHIFT:
			return { "SHIFT key" };
		case VK_CONTROL:
			return { "CTRL key" };
		case VK_MENU:
			return { "ALT key" };
		case VK_PAUSE:
			return { "PAUSE key" };
		case VK_CAPITAL:
			return { "CAPS LOCK key" };
		case VK_KANA:
			return { "IME Kana/Hangul mode" };
		case VK_IME_ON:
			return { "IME On" };
		case VK_JUNJA:
			return { "IME Junja mode" };
		case VK_FINAL:
			return { "IME final mode" };
		case VK_HANJA:
			return { "IME Hanja/Kanji mode" };
		case VK_IME_OFF:
			return { "IME Off" };
		case VK_ESCAPE:
			return { "ESC key" };
		case VK_CONVERT:
			return { "IME convert" };
		case VK_NONCONVERT:
			return { "IME nonconvert" };
		case VK_ACCEPT:
			return { "IME accept" };
		case VK_MODECHANGE:
			return { "IME mode change request" };
		case VK_SPACE:
			return { "SPACEBAR" };
		case VK_PRIOR:
			return { "PAGE UP key" };
		case VK_NEXT:
			return { "PAGE DOWN key" };
		case VK_END:
			return { "END key" };
		case VK_HOME:
			return { "HOME key" };
		case VK_LEFT:
			return { "LEFT ARROW key" };
		case VK_UP:
			return { "UP ARROW key" };
		case VK_RIGHT:
			return { "RIGHT ARROW key" };
		case VK_DOWN:
			return { "DOWN ARROW key" };
		case VK_SELECT:
			return { "SELECT key" };
		case VK_PRINT:
			return { "PRINT key" };
		case VK_EXECUTE:
			return { "EXECUTE key" };
		case VK_SNAPSHOT:
			return { "PRINT SCREEN key" };
		case VK_INSERT:
			return { "INS key" };
		case VK_DELETE:
			return { "DEL key" };
		case VK_HELP:
			return { "HELP key" };
		case '0': [[fallthrough]];
		case '1': [[fallthrough]];
		case '2': [[fallthrough]];
		case '3': [[fallthrough]];
		case '4': [[fallthrough]];
		case '5': [[fallthrough]];
		case '6': [[fallthrough]];
		case '7': [[fallthrough]];
		case '8': [[fallthrough]];
		case '9':
			return std::string { "Digit " } + char(key);
		case 0x3A: [[fallthrough]];
		case 0x3B: [[fallthrough]];
		case 0x3C: [[fallthrough]];
		case 0x3D: [[fallthrough]];
		case 0x3E: [[fallthrough]];
		case 0x3F: [[fallthrough]];
		case 0x40:
			return { "Undefined" };
		case 'A': [[fallthrough]];
		case 'B': [[fallthrough]];
		case 'C': [[fallthrough]];
		case 'D': [[fallthrough]];
		case 'E': [[fallthrough]];
		case 'F': [[fallthrough]];
		case 'G': [[fallthrough]];
		case 'H': [[fallthrough]];
		case 'I': [[fallthrough]];
		case 'J': [[fallthrough]];
		case 'K': [[fallthrough]];
		case 'L': [[fallthrough]];
		case 'M': [[fallthrough]];
		case 'N': [[fallthrough]];
		case 'O': [[fallthrough]];
		case 'P': [[fallthrough]];
		case 'Q': [[fallthrough]];
		case 'R': [[fallthrough]];
		case 'S': [[fallthrough]];
		case 'T': [[fallthrough]];
		case 'U': [[fallthrough]];
		case 'V': [[fallthrough]];
		case 'W': [[fallthrough]];
		case 'X': [[fallthrough]];
		case 'Y': [[fallthrough]];
		case 'Z':
			return std::string { "Letter " } + char(key);
		case VK_LWIN:
			return { "Left Windows key" };
		case VK_RWIN:
			return { "Right Windows key" };
		case VK_APPS:
			return { "Applications key" };
		case 0x5E:
			return { "Reserved" };
		case VK_SLEEP:
			return { "Computer Sleep key" };
		case VK_NUMPAD0:
			return { "Numeric keypad 0 key" };
		case VK_NUMPAD1:
			return { "Numeric keypad 1 key" };
		case VK_NUMPAD2:
			return { "Numeric keypad 2 key" };
		case VK_NUMPAD3:
			return { "Numeric keypad 3 key" };
		case VK_NUMPAD4:
			return { "Numeric keypad 4 key" };
		case VK_NUMPAD5:
			return { "Numeric keypad 5 key" };
		case VK_NUMPAD6:
			return { "Numeric keypad 6 key" };
		case VK_NUMPAD7:
			return { "Numeric keypad 7 key" };
		case VK_NUMPAD8:
			return { "Numeric keypad 8 key" };
		case VK_NUMPAD9:
			return { "Numeric keypad 9 key" };
		case VK_MULTIPLY:
			return { "Multiply key" };
		case VK_ADD:
			return { "Add key" };
		case VK_SEPARATOR:
			return { "Separator key" };
		case VK_SUBTRACT:
			return { "Subtract key" };
		case VK_DECIMAL:
			return { "Decimal key" };
		case VK_DIVIDE:
			return { "Divide key" };
		case VK_F1: [[fallthrough]];
		case VK_F2: [[fallthrough]];
		case VK_F3: [[fallthrough]];
		case VK_F4: [[fallthrough]];
		case VK_F5: [[fallthrough]];
		case VK_F6: [[fallthrough]];
		case VK_F7: [[fallthrough]];
		case VK_F8: [[fallthrough]];
		case VK_F9: [[fallthrough]];
		case VK_F10: [[fallthrough]];
		case VK_F11: [[fallthrough]];
		case VK_F12: [[fallthrough]];
		case VK_F13: [[fallthrough]];
		case VK_F14: [[fallthrough]];
		case VK_F15: [[fallthrough]];
		case VK_F16: [[fallthrough]];
		case VK_F17: [[fallthrough]];
		case VK_F18: [[fallthrough]];
		case VK_F19: [[fallthrough]];
		case VK_F20: [[fallthrough]];
		case VK_F21: [[fallthrough]];
		case VK_F22: [[fallthrough]];
		case VK_F23: [[fallthrough]];
		case VK_F24:
			return std::string { "Function key " } + std::to_string(key - VK_F1 + 1);
		case 0x88: [[fallthrough]];
		case 0x89: [[fallthrough]];
		case 0x8A: [[fallthrough]];
		case 0x8B: [[fallthrough]];
		case 0x8C: [[fallthrough]];
		case 0x8D: [[fallthrough]];
		case 0x8E: [[fallthrough]];
		case 0x8F:
			return { "Reserved" };
		case VK_NUMLOCK:
			return { "NUM LOCK key" };
		case VK_SCROLL:
			return { "SCROLL LOCK key" };
		case 0x92: [[fallthrough]];
		case 0x93: [[fallthrough]];
		case 0x94: [[fallthrough]];
		case 0x95: [[fallthrough]];
		case 0x96:
			return { "OEM specific" };
		case 0x97: [[fallthrough]];
		case 0x98: [[fallthrough]];
		case 0x99: [[fallthrough]];
		case 0x9A: [[fallthrough]];
		case 0x9B: [[fallthrough]];
		case 0x9C: [[fallthrough]];
		case 0x9D: [[fallthrough]];
		case 0x9E: [[fallthrough]];
		case 0x9F:
			return { "Unassigned" };
		case VK_LSHIFT:
			return { "Left SHIFT key" };
		case VK_RSHIFT:
			return { "Right SHIFT key" };
		case VK_LCONTROL:
			return { "Left CONTROL key" };
		case VK_RCONTROL:
			return { "Right CONTROL key" };
		case VK_LMENU:
			return { "Left ALT key" };
		case VK_RMENU:
			return { "Right ALT key" };
		case VK_BROWSER_BACK:
			return { "Browser Back key" };
		case VK_BROWSER_FORWARD:
			return { "Browser Forward key" };
		case VK_BROWSER_REFRESH:
			return { "Browser Refresh key" };
		case VK_BROWSER_STOP:
			return { "Browser Stop key" };
		case VK_BROWSER_SEARCH:
			return { "Browser Search key" };
		case VK_BROWSER_FAVORITES:
			return { "Browser Favorites key" };
		case VK_BROWSER_HOME:
			return { "Browser Start and Home key" };
		case VK_VOLUME_MUTE:
			return { "Volume Mute key" };
		case VK_VOLUME_DOWN:
			return { "Volume Down key" };
		case VK_VOLUME_UP:
			return { "Volume Up key" };
		case VK_MEDIA_NEXT_TRACK:
			return { "Next Track key" };
		case VK_MEDIA_PREV_TRACK:
			return { "Previous Track key" };
		case VK_MEDIA_STOP:
			return { "Stop Media key" };
		case VK_MEDIA_PLAY_PAUSE:
			return { "Play/Pause Media key" };
		case VK_LAUNCH_MAIL:
			return { "Start Mail key" };
		case VK_LAUNCH_MEDIA_SELECT:
			return { "Select Media key" };
		case VK_LAUNCH_APP1:
			return { "Start Application 1 key" };
		case VK_LAUNCH_APP2:
			return { "Start Application 2 key" };
		case 0xB8: [[fallthrough]];
		case 0xB9:
			return { "Reserved" };
		case VK_OEM_1:
			return { "Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the ;: key" };
		case VK_OEM_PLUS:
			return { "For any country/region, the + key" };
		case VK_OEM_COMMA:
			return { "For any country/region, the , key" };
		case VK_OEM_MINUS:
			return { "For any country/region, the - key" };
		case VK_OEM_PERIOD:
			return { "For any country/region, the . key" };
		case VK_OEM_2:
			return { "Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the /? key" };
		case VK_OEM_3:
			return { "Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the `~ key" };
		case 0xC1: [[fallthrough]];
		case 0xC2: [[fallthrough]];
		case 0xC3: [[fallthrough]];
		case 0xC4: [[fallthrough]];
		case 0xC5: [[fallthrough]];
		case 0xC6: [[fallthrough]];
		case 0xC7: [[fallthrough]];
		case 0xC8: [[fallthrough]];
		case 0xC9: [[fallthrough]];
		case 0xCA: [[fallthrough]];
		case 0xCB: [[fallthrough]];
		case 0xCC: [[fallthrough]];
		case 0xCD: [[fallthrough]];
		case 0xCE: [[fallthrough]];
		case 0xCF: [[fallthrough]];
		case 0xD0: [[fallthrough]];
		case 0xD1: [[fallthrough]];
		case 0xD2: [[fallthrough]];
		case 0xD3: [[fallthrough]];
		case 0xD4: [[fallthrough]];
		case 0xD5: [[fallthrough]];
		case 0xD6: [[fallthrough]];
		case 0xD7: [[fallthrough]];
		case 0xD8: [[fallthrough]];
		case 0xD9: [[fallthrough]];
		case 0xDA:
			return { "Reserved" };
		case VK_OEM_4:
			return { "Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the [{ key" };
		case VK_OEM_5:
			return { "Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the \\| key" };
		case VK_OEM_6:
			return { "Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the ]} key" };
		case VK_OEM_7:
			return { "Used for miscellaneous characters; it can vary by keyboard. For the US standard keyboard, the '\" key" };
		case VK_OEM_8:
			return { "OEM 8" };
		case VK_OEM_102:
			return { "<>" };
		case VK_PROCESSKEY:
			return { "IME PROCESS key" };
		case VK_PACKET:
			return { "Unicode character as keystroke" };
		case 0xE8:
			return { "Unassigned" };
		case VK_ATTN:
			return { "Attn key" };
		case VK_CRSEL:
			return { "CrSel key" };
		case VK_EXSEL:
			return { "ExSel key" };
		case VK_EREOF:
			return { "Erase EOF key" };
		case VK_PLAY:
			return { "Play key" };
		case VK_ZOOM:
			return { "Zoom key" };
		case VK_NONAME:
			return { "Reserved" };
		case VK_PA1:
			return { "PA1 key" };
		case VK_OEM_CLEAR:
			return { "Clear key" };
		default: {
			if (key >= 0xDF && key <= 0xFE) {
				return { "OEM Specific" };
			}

			return { "Unknown key" };
		}
	}
}

std::string msg_to_string(LPARAM lParam) noexcept {
	switch(lParam) {
	case WM_KEYDOWN: [[fallthrough]];
	case WM_SYSKEYDOWN:
		return { "down" };
	case WM_KEYUP: [[fallthrough]];
	case WM_SYSKEYUP:
		return { "up" };
	case WM_LBUTTONDOWN:
		return { "left button down" };
	case WM_LBUTTONUP:
		return { "left button up" };
	case WM_LBUTTONDBLCLK:
		return { "left button double click" };
	case WM_RBUTTONDOWN:
		return { "right button down" };
	case WM_RBUTTONUP:
		return { "right button up" };
	case WM_RBUTTONDBLCLK:
		return { "right button double click" };
	case WM_MBUTTONDOWN:
		return { "middle button down" };
	case WM_MBUTTONUP:
		return { "middle button up" };
	case WM_MBUTTONDBLCLK:
		return { "middle button double click" };
	case WM_MOUSEWHEEL:
		return { "mouse wheel" };
	case WM_MOUSEMOVE:
		return { "mouse move" };
	default:
		return { "" };
	}
}

} // capture_io
