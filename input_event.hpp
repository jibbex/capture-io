#ifndef INPUTEVENT_HPP
#define INPUTEVENT_HPP

#include <Windows.h>
#include <WinUser.h>
#include <cinttypes>
#include <string>

#include <QKeySequence>

namespace capture_io::events {

QKeySequence vk_to_keyseq(const std::uint32_t key) noexcept;
std::string vk_to_string(const std::uint32_t key) noexcept;
std::string msg_to_string(LPARAM lParam) noexcept;

struct Point {
	long x;
	long y;
};

class InputEvent {
public:
	InputEvent(std::uint64_t info, unsigned long flags, std::uint64_t msg) noexcept;
	virtual ~InputEvent();

	[[nodiscard]] constexpr std::uint64_t get_msg() const noexcept { return _msg; }
	[[nodiscard]] constexpr std::uint64_t get_extra_info() const noexcept { return _extra_info; }
	[[nodiscard]] constexpr unsigned long get_flags() const noexcept { return _flags; }

	virtual operator const std::string() noexcept;

private:
	std::uint64_t _msg;
	std::uint64_t _extra_info;
	unsigned long _flags;
};

class MouseEvent : public InputEvent {
public:
	MouseEvent(unsigned int info, unsigned long flags, std::uint64_t msg, unsigned long data, const Point& point) noexcept;
	MouseEvent(const MSLLHOOKSTRUCT& data, std::uint64_t msg) noexcept;

	[[nodiscard]] constexpr const Point& get_point() const noexcept { return _point; }
	[[nodiscard]] constexpr const std::uint64_t get_data() const noexcept { return _data; }

	operator const MSLLHOOKSTRUCT() noexcept;
	operator const std::string() noexcept override;

private:
	unsigned long _data;
	Point _point;
};

class KeyboardEvent : public InputEvent {
public:
	KeyboardEvent(unsigned int info, unsigned long flags, std::uint64_t msg, unsigned short vkey, unsigned short key_code) noexcept;
	KeyboardEvent(const KBDLLHOOKSTRUCT& data, std::uint64_t msg) noexcept;

	[[nodiscard]] constexpr unsigned short get_vkey() const noexcept { return _vkey; }
	[[nodiscard]] constexpr unsigned short get_key_code() const noexcept { return _key_code; }

	operator const KBDLLHOOKSTRUCT() noexcept;
	operator const std::string() noexcept override;

private:
	unsigned short _vkey;
	unsigned short _key_code;
};

} // capture_io

#endif // INPUTEVENT_HPP
