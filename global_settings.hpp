#ifndef GLOBALSETTINGS_HPP
#define GLOBALSETTINGS_HPP

#include <QKeySequence>


namespace capture_io {

class GlobalSettings {
public:
	constexpr GlobalSettings(const GlobalSettings&) = delete;
	constexpr GlobalSettings operator=(const GlobalSettings&) = delete;

	constexpr static auto APP_DIR = u8"capture-io";
	constexpr static int MAX_SPEED { 5 };

	[[nodiscard]] static GlobalSettings& instance() noexcept;

	[[nodiscard]] constexpr const int get_real_playback_speed() const noexcept {
		auto speed = MAX_SPEED - _playback_speed;
		return speed >= 0 ? speed : 0;
	}

	[[nodiscard]] constexpr const int get_playback_speed() const noexcept { return _playback_speed; }
	[[nodiscard]] const QKeySequence& get_play_hotkey() const noexcept;
	[[nodiscard]] const QKeySequence& get_rec_hotkey() const noexcept;
	void set_play_hotkey(const QKeySequence& seq) noexcept;
	void set_rec_hotkey(const QKeySequence& seq) noexcept;
	void set_playback_speed(int speed) noexcept;

private:
	GlobalSettings() noexcept;
	virtual ~GlobalSettings();

	void load();
	void save();

	int _playback_speed;
	QKeySequence _play_hotkey;
	QKeySequence _rec_hotkey;
};

}

#endif // GLOBALSETTINGS_HPP
