# Capture IO
Simple little helper, that records keyboard and mouse input and plays the recorded input in a loop back. Uses low level hooks of WinAPI, therefore it will only compile for Windows. Runs on maybe version 7, but certainly version 10 to latest.

## Dependencies
- Qt6
- Thats all

---
<div align="center">

> ### For my 
> <img src="https://git.michm.de/manne/capture-io/-/raw/main/assets/sun.png?ref_type=heads" alt="🌞">

</div>
