#include "global_settings.hpp"

#include <fstream>
#include <iostream>
#include <regex>
#include <filesystem>

#include <QMessageBox>

namespace {
using namespace std::filesystem;

path get_config_path(const char8_t* sub_dir) {
	char *buffer;
	size_t len;

	_dupenv_s(&buffer, &len, "APPDATA");

	path filepath { buffer };
	filepath.append(sub_dir);
	return filepath;
}

}

namespace capture_io {

GlobalSettings::GlobalSettings() noexcept
    : _playback_speed { 5 },
      _play_hotkey { "F4" },
      _rec_hotkey { "F12" } {
    load();
}

GlobalSettings::~GlobalSettings() {
    save();
}

GlobalSettings& GlobalSettings::instance() noexcept {
	static GlobalSettings s;
	return s;
}

const QKeySequence& GlobalSettings::get_play_hotkey() const noexcept {
	return _play_hotkey;
}

const QKeySequence& GlobalSettings::get_rec_hotkey() const noexcept {
	return _rec_hotkey;
}

void GlobalSettings::set_play_hotkey(const QKeySequence& seq) noexcept {
	_play_hotkey = seq;
}

void GlobalSettings::set_rec_hotkey(const QKeySequence& seq) noexcept {
	_rec_hotkey = seq;
}

void GlobalSettings::set_playback_speed(int speed) noexcept {
	_playback_speed = speed;
}


void GlobalSettings::load() {
	using namespace std::filesystem;
	auto filepath = get_config_path(APP_DIR);

	if (!exists(filepath)) {
		create_directories(filepath);
	}

	filepath.append("config.ini");

	std::fstream stream { filepath, stream.in };

	if (stream.is_open()) {
		for (std::string line; std::getline(stream, line);) {
			auto key = line.substr(0, line.find(" = "));
			auto value = line.substr(key.size() + 3);
			qDebug() << key << value;
			if (key == "play-hotkey") {
				_play_hotkey = QKeySequence { QString::fromStdString(value) };
			} else if (key == "rec-hotkey") {
				_play_hotkey = QKeySequence { QString::fromStdString(value) };
			} else if (key == "playback-speed") {
				std::regex digits { "\\d+" };
				std::smatch match {};

				if (std::regex_match(value, match, digits)) {
					_playback_speed = std::stoi(match.str());
				}
			}
		}

		stream.close();
	}
}

void GlobalSettings::save() {
	using namespace std::filesystem;
	auto filepath = get_config_path(APP_DIR);
	filepath.append("config.ini");

	std::fstream stream { filepath.string(), stream.out };

	if (stream.is_open()) {
		stream << "play-hotkey = " << _play_hotkey.toString().toStdString() << std::endl;
		stream << "rec-hotkey = " << _rec_hotkey.toString().toStdString() << std::endl;
		stream << "playback-speed = " << _playback_speed << std::endl;
		stream.close();
	} else {
		QString msg { "The file \"" + QString::fromStdString(filepath.string()) + "\" couldn't be opened." };
		QMessageBox::critical(nullptr, "Error", msg, QMessageBox::Ok, QMessageBox::Ok);

	}
}

}
