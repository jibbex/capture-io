#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "dlg_settings.hpp"
#include "input_event.hpp"

#include <Windows.h>
#include <WinUser.h>
#include <chrono>
#include <memory>
#include <vector>
#include <variant>

#include <QMainWindow>
#include <QListWidget>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

namespace capture_io {

using namespace events;
using namespace std::chrono;
using CaptureList = std::vector<std::variant<KeyboardEvent, MouseEvent>>;

class MainWindow : public QMainWindow {
	Q_OBJECT
public:
	MainWindow(const MainWindow&) = delete;
	MainWindow operator=(const MainWindow&) = delete;
	static MainWindow& instance() noexcept;
	enum State { PLAYING, RECORDING };
	CaptureList &get_records() noexcept;
	QListWidget *get_list() noexcept;

	[[nodiscard]] constexpr const bool is_playing() const noexcept { return _playing; }
	[[nodiscard]] constexpr const bool is_recording() const noexcept { return _recording; }
	[[nodiscard]] constexpr const microseconds get_last_toggle() const noexcept { return _last_toggle; }
	static constexpr const int DELAY_MS { 2000 };

	void set_last_toggle(const microseconds& ms) noexcept;

public slots:
	void set_playing(bool playing) noexcept;
	void set_recording(bool recording) noexcept;

signals:
	void playing_changed(bool playing);
	void recording_changed(bool playing);
	void update_label(const QString &text);
	void state_changed(MainWindow::State state);

private slots:
	void on_settings_clicked() noexcept;
	void on_tbToggleRec_clicked(bool checked) noexcept;
	void on_tbTogglePlay_clicked(bool checked) noexcept;
	void on_actionPlay_triggered(bool checked) noexcept;
	void on_actionRecord_triggered(bool checked) noexcept;
	void on_state_changed(MainWindow::State state) noexcept;

private:
	MainWindow(QWidget *parent = nullptr) noexcept;
	~MainWindow();
	void playback() noexcept;
	void install_hook() noexcept;
	void uninstall_hook() noexcept;
	static LRESULT kb_hook_proc(int nCode, WPARAM wParam, LPARAM lParam) noexcept;
	static LRESULT m_hook_proc(int nCode, WPARAM wParam, LPARAM lParam) noexcept;
	DlgSettings _settings_dlg;
	std::unique_ptr<Ui::MainWindow> _ui;
	CaptureList _records;
	bool _playing;
	bool _recording;
	HHOOK _kbHookId;
	HHOOK _mHookId;
	microseconds _last_toggle;
};

} // namespace capture_io
#endif // MAINWINDOW_HPP
