#include "mainwindow.hpp"
#include "./ui_mainwindow.h"
#include "global_settings.hpp"

#include <wtypes.h>

#include <QtConcurrent/QtConcurrent>

namespace capture_io {

MainWindow::MainWindow(QWidget *parent) noexcept
    : QMainWindow { parent },
      _settings_dlg {},
      _ui { std::make_unique<Ui::MainWindow>() },
      _records {},
      _playing {},
      _recording {},
      _kbHookId { nullptr },
      _mHookId { nullptr },
      _last_toggle { } {
    _ui->setupUi(this);
    _ui->tbTogglePlay->setDefaultAction(_ui->actionPlay);
    _ui->tbToggleRec->setDefaultAction(_ui->actionRecord);
    _kbHookId = SetWindowsHookEx(WH_KEYBOARD_LL, kb_hook_proc, nullptr, 0);

//	auto play_ico = QIcon { ":/icons/assets/play.png" };
//	auto rec_ico = QIcon { ":/icons/assets/record.png" };
//	auto conf_ico = QIcon { ":/icons/assets/settings.png" };
//	auto app_ico = QIcon { ":/icons/assets/sun.png" };

//	play_ico.addFile(":/icons/assets/stop.png", QSize {32, 32}, QIcon::Mode::Active);
//	rec_ico.addFile(":/icons/assets/stop.png", QSize {32, 32}, QIcon::Mode::Active);

//    _ui->tbTogglePlay->setIcon(play_ico);
//    _ui->tbToggleRec->setIcon(rec_ico);
//    _ui->tbSettings->setIcon(conf_ico);
//    _settings_dlg.setWindowIcon(conf_ico);

//    MainWindow::setWindowIcon(app_ico);
}

MainWindow::~MainWindow() {
	if (_kbHookId != nullptr) {
		UnhookWindowsHookEx(_kbHookId);
		_kbHookId = nullptr;
	}
    uninstall_hook();
}

MainWindow& MainWindow::instance() noexcept {
	static MainWindow w;
	return w;
}

void MainWindow::set_playing(bool playing) noexcept {
	if (!_recording) {
		_playing = playing;
	}
}

void MainWindow::set_recording(bool recording) noexcept {
	if (!_playing) {
		_recording = recording;
	}
}

void MainWindow::on_actionPlay_triggered(bool checked) noexcept {
	set_playing(checked);
	on_state_changed(State::PLAYING);
}

void MainWindow::on_actionRecord_triggered(bool checked) noexcept {
	set_recording(checked);
	on_state_changed(State::RECORDING);
}

void MainWindow::on_tbTogglePlay_clicked(bool checked) noexcept {
	set_playing(checked);
	on_state_changed(State::PLAYING);
}

void MainWindow::on_tbToggleRec_clicked(bool checked) noexcept {
	set_recording(checked);
	on_state_changed(State::RECORDING);
}

void MainWindow::on_state_changed(MainWindow::State state) noexcept {
	if (_recording) {
		install_hook();
		_records.clear();
		_ui->listWidget->clear();
	} else {
		uninstall_hook();
		if (State::RECORDING == state) {
			for (auto &ev : _records) {
				if(auto *kb = std::get_if<KeyboardEvent>(&ev)) {
					_ui->listWidget->addItem(QString::fromStdString(*kb));
				} else if (auto *mi = std::get_if<MouseEvent>(&ev)) {
					_ui->listWidget->addItem(QString::fromStdString(*mi));
				}
			}
		} else if (State::PLAYING == state && _playing) {
			playback();
			set_playing(false);
		}
	}

	_ui->tbToggleRec->setChecked(_recording);
	emit _ui->actionRecord->checkableChanged(_recording);
	_ui->tbTogglePlay->setChecked(_playing);
	emit _ui->actionPlay->checkableChanged(_playing);
}

void MainWindow::playback() noexcept {
	static RECT desktop;
	static const HWND dhandle = GetDesktopWindow();
	static constexpr double MAX { 0xFFFF };
	GetWindowRect(dhandle, &desktop);

	static const double WIDTH = desktop.right;
	static const double HEIGHT = desktop.bottom;
	for (auto ev : _records) {
		INPUT inputs[1] = {};
		ZeroMemory(inputs, sizeof(inputs));

		if(auto *kb = std::get_if<KeyboardEvent>(&ev)) {
			inputs[0].type = INPUT_KEYBOARD;
			inputs[0].ki.wVk = kb->get_vkey();
			inputs[0].ki.dwFlags = kb->get_msg() == WM_KEYUP ? KEYEVENTF_KEYUP : 0x0000;
		} else if (auto *mo = std::get_if<MouseEvent>(&ev)) {
			double x = mo->get_point().x;
			double y = mo->get_point().y;
			auto factorX = x / WIDTH;
			auto factorY = y / HEIGHT;
			x = std::round(MAX * factorX);
			y = std::round(MAX * factorY);
			inputs[0].type = INPUT_MOUSE;
			inputs[0].mi.dx = x;
			inputs[0].mi.dy = y;

			switch (mo->get_msg()) {
				case WM_LBUTTONDOWN:
					inputs[0].mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
					break;
				case WM_LBUTTONUP:
					inputs[0].mi.dwFlags = MOUSEEVENTF_LEFTUP;
					break;
				case WM_RBUTTONDOWN:
					inputs[0].mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
					break;
				case WM_RBUTTONUP:
					inputs[0].mi.dwFlags = MOUSEEVENTF_RIGHTUP;
					break;
				case WM_MBUTTONDOWN:
					inputs[0].mi.dwFlags = MOUSEEVENTF_MIDDLEDOWN;
					break;
				case WM_MBUTTONUP:
					inputs[0].mi.dwFlags = MOUSEEVENTF_MIDDLEUP;
					break;
				case WM_MOUSEWHEEL:
					inputs[0].mi.dwFlags = MOUSEEVENTF_WHEEL;
					break;
				case WM_MOUSEMOVE:
					inputs[0].mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;
					break;
			}
		}

		SendInput(ARRAYSIZE(inputs), inputs, sizeof(inputs));
		std::this_thread::sleep_for(std::chrono::microseconds(GlobalSettings::instance().get_real_playback_speed()));
	}
}

void MainWindow::install_hook() noexcept {
	if (_mHookId == nullptr) {
		_mHookId = SetWindowsHookExW(WH_MOUSE_LL, m_hook_proc, nullptr, 0);
	}
}

void MainWindow::uninstall_hook() noexcept {
	if (_mHookId != nullptr) {
		UnhookWindowsHookEx(_mHookId);
		_mHookId = nullptr;
	}
}
LRESULT MainWindow::kb_hook_proc(int nCode, WPARAM wParam, LPARAM lParam) noexcept {
	if (nCode < 0) {
		return CallNextHookEx(nullptr, nCode, wParam, lParam);
	}

	KBDLLHOOKSTRUCT *keyboard = reinterpret_cast<KBDLLHOOKSTRUCT *>(lParam);

	auto now = duration_cast<microseconds>(system_clock().now().time_since_epoch());
	auto &hk_play = GlobalSettings::instance().get_play_hotkey();
	auto &hk_rec = GlobalSettings::instance().get_rec_hotkey();
	auto seq = vk_to_keyseq(keyboard->vkCode);


	if (hk_play.matches(seq) == 2 && instance().get_last_toggle().count() + MainWindow::DELAY_MS < now.count()) {
		instance().on_tbTogglePlay_clicked(!instance().is_playing());
	}

//	if (hk_rec.matches(seq) == 2  && instance().get_last_toggle().count() + MainWindow::DELAY_MS < now.count()) {
//		instance().on_tbToggleRec_clicked(!instance().is_recording());
//	}

	if (hk_rec.matches(seq) == 2 || hk_play.matches(seq) == 2) {
		instance().set_last_toggle(now);
		return CallNextHookEx(nullptr, nCode, wParam, lParam);
	}

	if (instance().is_recording()) {
		instance().get_records().emplace_back(KeyboardEvent {*keyboard, wParam});
	}

	return CallNextHookEx(nullptr, nCode, wParam, lParam);
}

LRESULT MainWindow::m_hook_proc(int nCode, WPARAM wParam, LPARAM lParam) noexcept {
	if (nCode < 0) {
		return CallNextHookEx(nullptr, nCode, wParam, lParam);
	}

	MSLLHOOKSTRUCT *mouse = reinterpret_cast<MSLLHOOKSTRUCT *>(lParam);
	instance().get_records().emplace_back(MouseEvent {*mouse, wParam});
	return CallNextHookEx(nullptr, nCode, wParam, lParam);
}

void MainWindow::set_last_toggle(const microseconds& ms) noexcept {
	_last_toggle = ms;
}

CaptureList &MainWindow::get_records() noexcept {
	return _records;
}

QListWidget *MainWindow::get_list() noexcept {
	return _ui->listWidget;
}

void MainWindow::on_settings_clicked() noexcept {
	_settings_dlg.initialize();
	_settings_dlg.show();
}

} // namespace capture_io
