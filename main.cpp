#include "mainwindow.hpp"

#include <QApplication>
#include <QResource>

using namespace capture_io;

int main(int argc, char **argv) {
	QApplication a(argc, argv);
	QResource::registerResource("res.rss");
	a.setStyle("Fusion");
	MainWindow::instance().show();
    return a.exec();
}
