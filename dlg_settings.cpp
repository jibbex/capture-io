#include "dlg_settings.hpp"
#include "ui_dlg_settings.h"
#include "global_settings.hpp"

namespace capture_io {

DlgSettings::DlgSettings(QWidget *parent) :
      QDialog(parent),
      _ui { std::make_unique<Ui::DlgSettings>() } {
    _ui->setupUi(this);
    auto buttons = _ui->buttonBox->buttons();
    buttons[1]->setText("Cancel");
    _ui->speedSlider->setMaximum(GlobalSettings::MAX_SPEED);
    _ui->playHotkey->setMaximumSequenceLength(1);
//    _ui->recHotkey->setMaximumSequenceLength(1);
}

DlgSettings::~DlgSettings() {}

void DlgSettings::initialize() noexcept {
	_ui->speedSlider->setSliderPosition(GlobalSettings::instance().get_playback_speed());
	_ui->playHotkey->setKeySequence(GlobalSettings::instance().get_play_hotkey());
//	_ui->recHotkey->setKeySequence(GlobalSettings::instance().get_rec_hotkey());
}

void DlgSettings::accept() noexcept {
	GlobalSettings::instance().set_playback_speed(_ui->speedSlider->value());
	GlobalSettings::instance().set_play_hotkey(_ui->playHotkey->keySequence());
//	GlobalSettings::instance().set_rec_hotkey(_ui->recHotkey->keySequence());
	this->hide();
}


} // capture_io
