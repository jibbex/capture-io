#ifndef DLG_SETTINGS_HPP
#define DLG_SETTINGS_HPP

#include <QDialog>

namespace Ui {
class DlgSettings;
}


namespace capture_io {


class DlgSettings : public QDialog {
	Q_OBJECT

public:
	explicit DlgSettings(QWidget *parent = nullptr);
	~DlgSettings();

	void initialize() noexcept;

private slots:
	void accept() noexcept;

private:
	std::unique_ptr<Ui::DlgSettings> _ui;
};

} // capture_io
#endif // DLG_SETTINGS_HPP
